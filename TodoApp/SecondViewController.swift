//
//  SecondViewController.swift
//  TodoApp
//
//  Created by Jabeed on 18/07/19.
//  Copyright © 2019 Jabeed. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var laterTableVIew:UITableView!
    @IBOutlet weak var dateTimeLabel:UILabel!
    @IBOutlet weak var descrptionLabel:UILabel!
    
    var responseDictionary:NSMutableDictionary = NSMutableDictionary()
    
    let textCellIdentifier = "cellResuseIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        laterTableVIew.delegate = self;
        laterTableVIew.dataSource = self;
        
        
        dateTimeLabel.text! = (Utils.getTomorrowsDate() as NSString) as String
        descrptionLabel.text! = String(format: "There's a lot going on today.There are %d todos sceheduled, and the first one starts At %d", 0 , 0 )
        
        
        TodoUrlConnection.getTodoData(callbackHandler:{
            (error:NSError?,data:NSMutableArray?)  -> Void in
            DispatchQueue.main.async {
                if data != nil && (data!.count) > 0 {
                    var pendingArray:[NSDictionary] = []
                    var completedArray:[NSDictionary] = []
                    
                    for  currentItem in data! {
                        if  let obj = (currentItem as AnyObject) as? NSDictionary {
                            if obj["status"]! as! String == "PENDING" {
                                pendingArray.append(obj)
                            } else {
                                completedArray.append(obj)
                            }
                        }
                    }
                    
                    self.responseDictionary["pendingArray"] = pendingArray
                    self.responseDictionary["completedArray"] = completedArray
                    
                    if pendingArray.count > 0 {
                        self.descrptionLabel.text! = String(format: "There's a lot going on today.There are %d todos sceheduled, and the first one starts At %@", pendingArray.count , (Utils.getTomorrowsDate() as NSString) as String)
                    }
                    
                    
                    
                    self.laterTableVIew.reloadData()
                }
            }
        })
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if self.responseDictionary.allKeys.count > 0 {
                return (self.responseDictionary["pendingArray"]! as AnyObject).count
            } else {
                return 0
            }
            
        } else {
            if self.responseDictionary.allKeys.count > 0 {
                return (self.responseDictionary["completedArray"]! as AnyObject).count
            } else {
                return 0
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Pending"
        } else {
            return "Completed"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as! TodoTableViewCell
        
        let row = indexPath.row
        var currentDictionary:NSDictionary = NSDictionary()
        
        switch indexPath.section {
        case 0:
            currentDictionary = ((self.responseDictionary["pendingArray"]! as! NSArray)[row] as! NSDictionary)
            break
        case 1:
            currentDictionary = ((self.responseDictionary["completedArray"]! as! NSArray)[row] as! NSDictionary)
            cell.statusSwitchBox?.isOn = false
            cell.statusSwitchBox!.isEnabled = false
            break
        default: break
            
        }
        
        cell.descriptionLabel?.text = currentDictionary["description"] as! String?
        cell.scheduleDateLabel?.text = (Utils.getTomorrowsDate() as NSString) as String
        
        return cell
    }
    
}
