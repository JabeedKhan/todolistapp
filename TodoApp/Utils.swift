//
//  Utils.swift
//  TodoApp
//
//  Created by Jabeed on 19/07/19.
//  Copyright © 2019 Jabeed. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
   class  func getConvertedDateString(dateString:String!) -> NSString? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmm"
        dateFormatter.timeZone = TimeZone.current
        let dateObject = dateFormatter.date(from:dateString!)!
        dateFormatter.dateFormat = "MMM dd";
        let convertedStringDate =  dateFormatter.string(from: dateObject)
        return convertedStringDate as NSString
    }
    
    class func getTodaysDate() -> NSString {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd"
        return formatter.string(from: date) as NSString

    }
    
    class func getTomorrowsDate() -> NSString {
        let now = Calendar.current.dateComponents(in: .current, from: Date())
        let tomorrow = DateComponents(year: now.year, month: now.month, day: now.day! + 1)
        let dateTomorrow = Calendar.current.date(from: tomorrow)!
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd"
        return formatter.string(from: dateTomorrow) as NSString

    }

}
