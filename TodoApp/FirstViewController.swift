//
//  FirstViewController.swift
//  TodoApp
//
//  Created by Jabeed on 18/07/19.
//  Copyright © 2019 Jabeed. All rights reserved.
//

import UIKit

class CustomSwitchBox:UISwitch {
    var indexPath:NSIndexPath?
}


class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var todayTableVIew:UITableView!
    @IBOutlet weak var dateTimeLabel:UILabel!
    @IBOutlet weak var descrptionLabel:UILabel!
    
    @IBAction func swithboxClicked(sender:CustomSwitchBox) {
        var currentDictionary:NSDictionary = NSDictionary()
        switch sender.indexPath!.section {
        case 0:
            currentDictionary = ((self.responseDictionary["pendingArray"]! as! NSArray)[sender.indexPath!.row] as! NSDictionary)
            
            if  currentDictionary["status"] as! String == "PENDING" {
                let clonedObject = currentDictionary.mutableCopy()
                (clonedObject as! NSMutableDictionary).setValue("COMPLETED", forKey: "status")
                
                let completedArray = self.responseDictionary["completedArray"]! as! NSMutableArray
                completedArray.add(clonedObject)
                
                let pendingArray = self.responseDictionary["pendingArray"]! as! NSMutableArray
                if pendingArray.count > 0 {
                    pendingArray.removeObject(at: sender.indexPath!.row)
                }
                
                self.todayTableVIew.reloadData()
                
            }
            
            break
        case 1:
            currentDictionary = ((self.responseDictionary["completedArray"]! as! NSArray)[sender.indexPath!.row] as! NSDictionary)
            
            if  currentDictionary["status"] as! String == "COMPLETED" {
                let clonedObject = currentDictionary.mutableCopy()
                (clonedObject as! NSMutableDictionary).setValue("PENDING", forKey: "status")
                
                let pendingArray = self.responseDictionary["pendingArray"]! as! NSMutableArray
                pendingArray.add(clonedObject)
                
                let completedArray = self.responseDictionary["completedArray"]! as! NSMutableArray
                if completedArray.count > 0 {
                    completedArray.removeObject(at: sender.indexPath!.row)
                }
                
                self.todayTableVIew.reloadData()
                
            }
            break
        default:
            break
        }
    }
    
    var responseDictionary:NSMutableDictionary = NSMutableDictionary()
    
    let textCellIdentifier = "cellResuseIdentifier"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        todayTableVIew.delegate = self;
        todayTableVIew.dataSource = self;
        
        
        dateTimeLabel.text! = Utils.getTodaysDate() as String
        descrptionLabel.text! = String(format: "There's a lot going on today.There are %d todos sceheduled, and the first one starts At %d", 0 , 0 )
        
        
        TodoUrlConnection.getTodoData(callbackHandler:{
            (error:NSError?,data:NSMutableArray?)  -> Void in
            DispatchQueue.main.async {
                if data != nil && (data!.count) > 0 {
                    var pendingArray:NSMutableArray = []
                    var completedArray:NSMutableArray = []
                    
                    for  currentItem in data! {
                        if  let obj = (currentItem as AnyObject) as? NSDictionary {
                            if obj["status"]! as! String == "PENDING" {
                                pendingArray.add(obj)
                            } else {
                                completedArray.add(obj)
                            }
                        }
                    }
                    
                    //Now sort the arrays by description in ascending order
                    let sortedPendingArray = pendingArray.sorted(by: { ($0 as! NSDictionary).description < ($1 as! NSDictionary).description })
                    
                    pendingArray =  (sortedPendingArray as NSArray).mutableCopy() as! NSMutableArray
                    
                    let sortedCompletedArray = completedArray.sorted(by: { ($0 as! NSDictionary).description < ($1 as! NSDictionary).description })
                    
                    completedArray =  (sortedCompletedArray as NSArray).mutableCopy() as! NSMutableArray
                    
                    self.responseDictionary["pendingArray"] = pendingArray
                    self.responseDictionary["completedArray"] = completedArray
                    
                    if pendingArray.count > 0 {
                        let firstRecord = pendingArray[0] as! NSDictionary;
                        let scheduleString =  firstRecord["scheduledDate"]
                        self.descrptionLabel.text! = String(format: "There's a lot going on today.There are %d todos sceheduled, and the first one starts At %@", pendingArray.count , Utils.getConvertedDateString(dateString: scheduleString! as? String)!)
                    }
                    
                    
                    
                    self.todayTableVIew.reloadData()
                }
            }
        })

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if self.responseDictionary.allKeys.count > 0 {
                return (self.responseDictionary["pendingArray"]! as AnyObject).count
            } else {
                return 0
            }
            
        } else {
            if self.responseDictionary.allKeys.count > 0 {
                return (self.responseDictionary["completedArray"]! as AnyObject).count
            } else {
                return 0
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Pending"
        } else {
            return "Completed"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as! TodoTableViewCell
        
        let row = indexPath.row
        var currentDictionary:NSDictionary = NSDictionary()
        
        switch indexPath.section {
        case 0:
             currentDictionary = ((self.responseDictionary["pendingArray"]! as! NSArray)[row] as! NSDictionary)
             cell.statusSwitchBox?.isOn = true
            break
        case 1:
            currentDictionary = ((self.responseDictionary["completedArray"]! as! NSArray)[row] as! NSDictionary)
            cell.statusSwitchBox?.isOn = false
            break
        default: break
            
        }
        
        cell.statusSwitchBox!.indexPath = indexPath as NSIndexPath
        
        cell.descriptionLabel?.text = currentDictionary["description"] as! String?
        cell.scheduleDateLabel?.text = Utils.getConvertedDateString(dateString:currentDictionary["scheduledDate"] as! String? ) as String?

        return cell
    }
    
}

