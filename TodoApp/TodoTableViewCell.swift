//
//  TodoTableViewCell.swift
//  TodoApp
//
//  Created by Jabeed on 18/07/19.
//  Copyright © 2019 Jabeed. All rights reserved.
//

import UIKit

class TodoTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel:UILabel!
    @IBOutlet weak var scheduleDateLabel:UILabel!
    @IBOutlet weak var statusSwitchBox:CustomSwitchBox!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
