//
//  TodoUrlConnection.swift
//  TodoApp
//
//  Created by Jabeed on 18/07/19.
//  Copyright © 2019 Jabeed. All rights reserved.
//

import UIKit

class TodoUrlConnection: NSURLConnection {

    class  func getTodoData(callbackHandler:((NSError?, NSMutableArray?) -> Void)!){
        let urlString = String(format:"http://www.mocky.io/v2/582695f5100000560464ca40")

        let query  = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        var request = URLRequest(url:URL(string:query!)!)
        request.timeoutInterval = 180
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if data != nil{
                do {
                    let json = try JSONSerialization.jsonObject(with:data!, options: [])
                    let jsonArray = NSMutableArray(array: json as! NSArray)
                    callbackHandler(nil,jsonArray)
                } catch let error as NSError {
                    callbackHandler(error, nil)
                }
            }else{
                callbackHandler(error! as NSError, nil)
            }
        })
        task.resume()
    }
}
